# QuickStart Guide Lab PPW Fasilkom UI 2020 #
QuickStart Guide ini bertujuan untuk membantu mempelajari hal yang dibutuhkan untuk mengerjakan tugas Lab PPW (Story) 

#### Note ####
Quick Start Guide ini hanya sebatas membantu menjelaskan step dalam proses pengerjaan tugas lab dan tidak bermaksud memberikan hasil akhir tugas setiap lab

Dalam QuickStart Guide ini, akan dibagi berdasarkan urutan tiap lab, dan memungkinkan berkaitan antara satu lab dengan lab yang lain.