# Lab 1 PPW #
Berikut merupakan link tugas Lab 1 PPW

https://docs.google.com/document/d/1qjQOc51KjoJMTiKw8-GrRCc6Eo57PHUOxbpVooStzcM/edit

## Penjelasan Singkat Tugas ##
Dalam tugas lab ini kita dikenalkan dengan website sederhana yang dapat menampilkan teks dan gambar.

Kita diminta untuk membuat website kita sendiri yang menampilkan biodata kita beserta dengan gambar foto profil. 
Untuk menampilkan website kita di internet, kita harus men-deploynya ke Heroku.

## Persiapan Sebelum Mulai ##
- Pastikan laptop Anda telah terinstall python minimal versi 3
- Pastikan laptop Anda telah terinstall git https://git-scm.com/downloads
- Buatlah akun GitLab https://gitlab.com/ 
- Buatlah akun Heroku https://www.heroku.com/

## Mempersiapkan Django Untuk Mengelola Halaman Website yang Kita Miliki ##
Penting : Mohon untuk membaca setiap baris code dan berusaha untuk dipahami dan tidak asal copy-paste

1. Buat repository baru di GitLab dengan login ke akun GitLab Anda dan klik new project

![Image of tutorial](http://andrewsusanto.id/quickstartlab-image/lab-1/1.PNG)

2. Masukkan project name sesuai nama yang diinginkan, pilih visibility level menjadi public, dan JANGAN centang Initialize repository with a README 

![Image of tutorial](http://andrewsusanto.id/quickstartlab-image/lab-1/2.PNG)

3. Setelah itu buka project yang sudah dibuat dan klik clone kemudian copy link yang ada pada kolom clone with HTTPS (link akan digunakan nanti)

![Image of tutorial](http://andrewsusanto.id/quickstartlab-image/lab-1/3.PNG)

4. Buka Command Prompt atau Terminal lalu install library virtualenv untuk python di laptop Anda

```pip install virtualenv```

5. Lalu arahkan terminal Anda ke folder Anda ingin menaruh file project lab1 ppw ini

Gunakan beberapa command dibawah ini :

`dir` = untuk melihat file yang ada di folder saat ini (Windows)

`ls` = untuk melihat file yang ada di folder saat ini (Mac)

`cd namafolder` = untuk membuka / masuk ke folder yang bernama 'namafolder'

`cd ..` = untuk keluar dari folder tersebut (kembali ke parent directory)

`cd /` = untuk keluar dari folder tersebut (kembali ke folder paling luar / root directory)

6. Gunakan virtual environment dengan memasukkan kode ini

```virtualenv venv ```

7. Setelah selesai, aktifkan virtual environment dengan memasukkan

```venv\Scripts\activate```

8. Selanjutnya, menginstall django dengan pip

```pip install django```

9. Setelah itu buat project dengan django dengan memasukkan

```django-admin startproject namaproject```

10. Masuk ke folder yang dibuat secara otomatis setelah Anda membuat project 

```cd namaproject```

11. Setelah itu buka folder namaproject Anda dengan code editor yang biasa Anda gunakan 
Didalam folder tersebut sudah terdapat beberapa file yaitu manage.py dan folder yang bernama namaproject Anda

12. Buka lagi folder namaproject yang berada di dalam folder namaproject Anda

13. Buat file bernama `views.py` yang berisi :

```
from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    return render(request,"index.html")
```

14. Selanjutnya anda harus mengedit file `urls.py` dengan menambahkan path dan menjadi seperti ini:

```
from django.contrib import admin
from django.urls import path
from .views import index

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',index)
]
```

15. Anda juga harus mengedit file `settings.py` dengan menambahkan beberapa baris kode :

```
import os
import django_heroku #tambahkan ini

...
# ubah variable MIDDLEWARE yang sudah ada menjadi seperti ini
MIDDLEWARE = [
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

...

# ubah variable TEMPLATES yang sudah ada menjadi seperti ini
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR,"template")],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

...
#Tambhakan kode dibawah ini pada bagian akhir dari settings.py

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')
STATIC_URL = '/static/'

# Extra places for collectstatic to find static files.
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'),
]

# Activate Django-Heroku.
django_heroku.settings(locals())


# Simplified static file serving.
# https://warehouse.python.org/project/whitenoise/

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

```

16. Kemudian Anda sudah dapat keluar dari folder project tersebut ke folder diluarnya yang berisi folder namaproject Anda dan manage.py

17. Tambahkan beberapa folder yang bernama `static` dan `template`, sehingga sekarang terdapat 3 buah folder(namaproject, static, dan template) dan 1 file (manage.py)

18. Dalam folder static buat file bernama `style.css`(Usahakan buat file melalui code editor agar extension sesuai

19. Dalam folder template buat file bernama `index.html` yang berisi :
```
{% load static %}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{% static 'style.css' %}" />
    <title>Document</title>
</head>
<body>
    Selamat Anda berhasil mendeploy web ke heroku
</body>
</html>
```

20. Setelah itu Anda perlu melakukan install terhadap beberapa library python, masukkan kode ini pada cmd atau terminal Anda satu per satu

`pip install django-heroku`

`pip install gunicorn`

`pip install whitenoise`

21. Setelah itu Anda perlu membuat file requirements.txt yang berisi library, tetapi pekerjaan Anda dimudahkan dengan pip, cukup mengetikan kode dibawah ini pada cmd atau terminal Anda

`pip freeze > requirements.txt`

Tambahkan satu baris dibawah ini dalam requirements.txt yang telah dibuat tadi

`psycopg2==2.7.5`

22. Kemudian Anda harus membuat file bernama `Procfile` (Usahakan membuat file dalam code editor agar extension benar), yang berisi:

`web: gunicorn [namaproject].wsgi`

*Ganti namaproject dengan nama project django Anda , tanpa kurung siku

23. Setelah ini , Anda dapat membuka web heroku dan membuat app baru dengan mengklik tombol new -> create new app pada heroku dashboard

24. Masukkan nama app yang Anda inginkan dan klik tombol Create App

25. Kemudian Anda harus membuat file bernama `.gitlab-ci.yml` (Usahakan membuat file dalam code editor agar extension benar), yang berisi:

```
image: python:3.7

stages:
  - ver
  - init
  - tests
  - deploy

ver:
  stage: ver
  script:
    - python --version
    - whoami

init:
  stage: init
  script:
    - pip3 install psycopg2-binary
    - pip3 install -r requirements.txt

run_tests:
  stage: tests
  script:
    - pip3 install psycopg2-binary
    - pip3 install -r requirements.txt
    - python3 manage.py test

deploy_staging:
  stage: deploy
  script:
    - git remote add heroku https://heroku:$HEROKU_API_KEY@git.heroku.com/$NAMA_GIT
    - git checkout -B "$CI_BUILD_REF_NAME" "$CI_BUILD_REF"
    - git push heroku 
  environment:
    name: staging
    url: $URL_HEROKU
  only:
    - master

deploy_production:
  stage: deploy
  script:
    - git remote add heroku https://heroku:$HEROKU_API_KEY@git.heroku.com/$NAMA_GIT
    - git checkout -B "$CI_BUILD_REF_NAME" "$CI_BUILD_REF"
    - git push heroku
  environment:
    name: production
    url: $URL_HEROKU
  when: manual
  only:
    - master

```

26. Setelah itu Anda bisa memindahkan beberapa variabel dari heroku ke gitlab

Cari variabel yang bernama API Key pada Heroku (Klik logo profile di kanan atas -> klik pada Acoount Setting) Scroll kebawah dan klik reveal, copy API Key tersebut ke Setting di GitLab(Buka project Anda->klik Setting->CI/CD->Variables), klik expand dan masukkan key `HEROKU_API_KEY` dan valuenya adalah API Key yang didapat dari heroku tadi. 

Kemudian buka app heroku, lalu masuk ke bagian setting dan cari Heroku Git URL dan copy BAGIAN AKHIR SAJA SEBELUM / (Slash) sehingga menjadi apalahnamaappnya.git , lalu copy ke gitlab dan tambahkan variable dan masukkan key `NAMA_GIT` dan valuenya adalah apalahappnamanya.git tadi. 

Kemudian buka app heroku, lalu cari bagian domains, copy link website anda yang pada umummnya https://apalahnamaappnya.herokuapp.com , lalu copy ke gitlab dan tambahkan variable dan masukkan key `URL_HEROKU` dan valuenya adalah link tersebut.

27. Setelah itu Anda dapat menaruh file ini di gitlab dengan cara

`git init`

`git remote add origin [linkgitdaristep3]`

`git add .`

`git commit -m "initial commit"`

`git push origin master`

28. Lalu tunggu sekitar 5 menit / sampai pipeline Anda selesai, atau Anda bisa melihat proses pipeline dari GitLab ke Heroku melalui menu CI/CD pada project GitLab, sekarang web Anda sudah dapat tampil di Heroku

29. Anda dapat mengupdate index.html sesuai dengan selera Anda (dan tentunya mengikuti ketentuan) dengan bantuan https://www.w3schools.com/


## Refference ##
https://dev.to/ruanbekker/gitlab-ci-cd-pipeline-to-deploy-your-python-api-with-postgres-on-heroku-12g3

https://gitlab.com/python-devs/ci-images

https://devcenter.heroku.com/articles/django-app-configuration

https://devcenter.heroku.com/articles/django-assets

https://gitlab.com/gitlab-org/gitlab-foss/issues/19421
